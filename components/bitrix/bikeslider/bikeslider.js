$(document).ready(function(){

function slicerbike(){
	$('.bikeslider_section').not('.slick-initialized').slick({
		slidesToShow: 1,
		centerMode: true,
		centerPadding: '50px',
		mobileFirst:true,
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		variableWidth: true,
		infinite: false,
		dots: true,
		responsive: [
		{
			breakpoint: 375,
			settings: "unslick"
		}
		]
	});

};

slicerbike();

$(window).resize(function(){

	slicerbike();
});
});




