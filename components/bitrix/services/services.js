$(document).ready(function(){

function slicerserv(){
	$('.service_points').not('.slick-initialized').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		centerMode: true,
		arrows: false,
		mobileFirst:true,
		variableWidth: true,
		infinite: true,
		responsive: [
		{
			breakpoint: 1000,
			settings: "unslick"
		},
		{
			breakpoint: 375,
			settings: {
				slidesToShow: 4,
				slidesToScroll: 2,
				centerMode: false
			}
		}
		]
	});

};
	slicerserv();
$(window).resize(function(){
	slicerserv();
});
});
