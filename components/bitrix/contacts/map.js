var myMap;

ymaps.ready(init);

function init () {
    myMap = new ymaps.Map('map', {
        center: [59.93, 30.31], 
        zoom: 16,
        controls: []
    }, {
        searchControlProvider: 'yandex#search'
    });
    myMap.behaviors.disable('scrollZoom');
    myMap.controls.add('fullscreenControl');
    myMap.controls.add('zoomControl', {
        float: 'none',
        position: {
            top: 100,
            right: 10   
        }
    }); 
    myGeoObject = new ymaps.GeoObject({
        geometry: {
            type: "Point",// тип геометрии - точка
            coordinates: [59.93, 30.31] // координаты точки
        }
    });
    myMap.geoObjects.add(myGeoObject);
}

