$(function() {

function window_manage(window_name, window_wrap){

	$(document).mouseup(function (e){ 
		if (!window_wrap.is(e.target) && window_wrap.has(e.target).length === 0) { 
			window_name.addClass('visuallyhidden');
			window_name.one('transitionend', function(e) {
				window_name.addClass('hidden');
			});
		}
	});
	
	if (window_name.hasClass('hidden')) {
		window_name.removeClass('hidden');
		setTimeout(function () {
			window_name.removeClass('visuallyhidden');
		}, 20);
	} else {
		window_name.addClass('visuallyhidden');
		window_name.one('transitionend', function(e) {
			window_name.addClass('hidden');
		});
	}
}


/*
Window manage Usage:  

$('.button_name').click(function(){
	window_manage($('.window_name'), &('.имя блока объединяещего кнопку с окном'));
});


.hidden
	display: none
.visuallyhidden
	opacity: 0
.window_name
	transition: 0.2s

*/


//Bucket_preview
$('.bucket_btn').click(function(){
	if($('.product_preview').length == 1){
		window_manage($('.bucket_preview') , $('.bucket_wrap'));
		$('.bucket_preview').addClass('one_element');
		$('.bucket_preview').removeClass('scroll_element');
	}else if($('.product_preview').length == 2){
		window_manage($('.bucket_preview') , $('.bucket_wrap'));
		$('.bucket_preview').removeClass('one_element');
		$('.bucket_preview').removeClass('scroll_element');
	}else if($('.product_preview').length > 2){
		window_manage($('.bucket_preview') , $('.bucket_wrap'));
		$('.bucket_preview').removeClass('one_element');
		$('.bucket_preview').addClass('scroll_element');
	}else{
		console.log('Корзина пуста');
	}
});






//mobilemenu

$('.mobile_menu_toggle').click(function(){
	if ($('header').hasClass('active')) {
		$('header').removeClass('active');
	} else {
		$('header').addClass('active');
	}
});

$(window).resize(function(){
	if ($(window).width() > 375){
		console.log('more than 375');
		$('header').removeClass('active');
	}
});





});
